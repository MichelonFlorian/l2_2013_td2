# Instructions

- Déposer un fichier du type `nom-prenom.txt` sur ce dépôt
- Le fichier doit contenir votre votre nom, votre prenom et votre email
- Un fichier par personne

Un example de fichier est [accessible ici](https://bitbucket.org/vjousse/l2_2013_td2/src/4a020685000f45bc52304ca5a069dc406e9b3512/jousse-vincent.txt?at=master).

Vous aurez certainement besoin des commandes suivantes :

- `git clone`
- `git add`
- `git commit`
- `git pull`
- `git push`
